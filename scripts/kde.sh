#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c India -a 6 --sort rate --save /etc/pacman.d/mirrorlist

git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si --noconfirm

sudo pacman -S --noconfirm xorg sddm plasma obs-studio vlc kdenlive dolphin kitty kvantum-qt5 ark gwenview 

sudo systemctl enable sddm

yay -S --noconfirm google-chrome optimus-manager optimus-manager-qt pfetch visual-studio-code-bin ttf-nerd-fonts-sauce-code-pro
