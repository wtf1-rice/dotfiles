#!/bin/sh
ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
hwclock --systohc

sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:password | 9499

pacman -S grub efibootmgr networkmanager wpa_supplicant mtools dosfstools reflector base-devel linux-headers xdg-user-dirs xdg-utils inetutils pipewire pipewire-alsa pipewire-pulse acpid os-prober fish

pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=x86_64-efi --bootloader-id=grub_uefi
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable tlp 
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable acpid

useradd -m akshant
echo akshant:password | 9499
usermod -aG wheel akshant

echo "akshant ALL=(ALL) ALL" >> /etc/sudoers.d/akshant

printf "Done"
