#!/usr/bin/env bash 
lxsession &
picom --experimental-backends --backend glx &
nitrogen --restore &
